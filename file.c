#include <stdio.h>		/* FILE, fprintf */
#include <string.h>     /* strcmp */
#include "file.h"
#include "terminate.h"

#define LINE_LENGTH 127

/*****
 * open file and return input file stream
 *****/
void open_file( FILE **rstream, char *file_name)
{
	if ((*rstream = fopen( file_name, "r")) == NULL)
		terminate( "Cannot open file.");
}

static void read_line( char line[], FILE *rstream)
{
	int ch, i = 0;
	ch = getc( rstream);
	while (ch != EOF && ch != '\n') {
		if (i < LINE_LENGTH)
			line[ i++] = ch;
		ch = getc( rstream);
	}
	if (ch == EOF)
		terminate( "Error in read_resolution: pbm file too short.");
	line[ i] = '\0';
}

/******
 * read_resolution: Read and return resolution of pixels from pbm input file.
 * In the meanwhile, perform the necessary acrobatics to check if the input
 * file is in correct pbm format.
 ******/
void read_resolution( FILE *rstream, int *col_count, int *row_count)
{
	char buff[3],
		line[ LINE_LENGTH];
	if (fgets( buff, 3, rstream) == NULL)
		terminate("Error in read_resolution: Empty or too short pbm File.");
	if (strcmp( buff, "P1") != 0)
		terminate( "Error in read_resolution: pbm file has to start with \"P1\".");
	read_line( line, rstream);
	sscanf( line, "%1s", buff);
	if (buff[0] != '#' && buff[0] != 'P')
		terminate( "Error in read_resolution: Not a pbm file.");
	/* consume comment lines however many they are */
	read_line( line, rstream);
	sscanf( line, "%1s", buff);
	while (buff[0] == '#') {
		read_line( line, rstream);
		sscanf( line, "%1s", buff);
	}
	if (sscanf( line, "%d%d", col_count, row_count) != 2)
		terminate( "Error in read_resolution: could not read height or width.");
	if (*col_count < 0 || *row_count < 0)
		terminate( "Negative height or width!");
}

/*****
 * close input file stream
 *****/
void close_file_stream( FILE *rstream)
{
	if (fclose( rstream) == EOF)
		terminate( "Cannot close file stream");
}

void create_file( FILE **wstream, char *file_name)
{
	if ((*wstream = fopen( file_name, "w")) == NULL)
		terminate( "Cannot create an output file.");
}
