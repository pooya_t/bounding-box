# Multi-threaded Bounding Box #

This program draws bounding boxes around all large enough objects in a
PBM image.

There is a PDF report in the current directory that explains the
approach and code design, and presents results and discussion.

## Run Program ##

Issue the following to a Linux shell command line:

```bash
$ make
$ ./bounding_box <PBM_image_name> <x>
```

where x implies 2^x concurrent threads will run the program.  The
results for x = 0, 1, 2, 3, 4 are presented in REPORT.pdf.

### Image File ###

Two sample PBM images, SMALL.pbm and LARGE.pbm, are given in the
current directory.

### Parameters ###

You can play with the value of `PROX_RANGE` and `REGION_SIZE` in
`constants.h` file, and look how the output changes.  The default values
are 3 and 100, respectively.

## Code ##

The function main is located inside bounding_box.c

This project is written in C to take advantage of the pthread library
to run a multi-threaded program at a very low level.
