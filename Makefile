# ****************************
# Makefile for Project 2 -- Bounding Box
# CS 5420 - Operating Systems
# Pooya Taherkhani
# pt376511 @ ohio edu
# Mon Feb 26 20:07:37 EST 2018
# ****************************

CC = gcc
DEBUG =
# DEBUG = -g  # To debug using gdb uncomment this line and comment out -O2 optimization flag
LIB = -lm -pthread
FLAGS = -Wall # -Werror -O2 # -std=c++11 
# FLAGS = -Wall -Wextra -Wpedantic -Werror #-O2 #-std=c++11 
CFLAGS = $(FLAGS) -c $(DEBUG)
LFLAGS = $(FLAGS) $(DEBUG) $(LIB)
EXEC = bounding_box
OBJS = file.o argc.o image.o terminate.o regions_list.o sections_list.o	\
tracker_matrix.o
HDRS = file.h argc.h image.h terminate.h regions_list.h sections_list.h	\
tracker_matrix.h constants.h visit_const.h

$(EXEC): $(EXEC).c $(OBJS) Makefile
	$(CC) $(OBJS) $(EXEC).c $(LFLAGS) -o $@

%.o: %.c $(HDRS) Makefile
	$(CC) $(CFLAGS) $< -o $@

clean:
	$(RM) *.o *~ # $(EXEC) *.new
