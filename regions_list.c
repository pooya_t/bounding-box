#include <stdlib.h>             /* malloc */
#include <stdio.h>              /* printf */
#include <stdbool.h>
#include "regions_list.h"
#include "terminate.h"
#include "constants.h"

struct region {
	int pixel_count,
		x_min, y_min,
		x_max, y_max;
};

struct region_node {
	Region data;
	struct region_node *next;
};

struct regions_list {
	struct region_node *head;
	struct region_node *cur_node;
	int count;
};

/******
 * create_region:  initialize all elements of region data to zero
 ******/
Region create_region()
{
	Region d = malloc( sizeof( struct region));
	if (d == NULL)
		terminate( "Error in create region: Could not create new region.");
	d->pixel_count = 0;
	d->x_min = 0;  d->y_min = 0;
	d->x_max = 0;  d->y_max = 0;
	return d;
}

/* void destroy_region( Region rgn) */
/* It MIGHT have been better if we defined a destroy_region function and called
 * it somewhere under destroy_regions_list (instead of directly calling free)*/

void set_pixel_count( Region dat, int pixel_cnt)
{
	dat->pixel_count = pixel_cnt;
}

void set_rgn_x_min( Region dat, int x_mn) { dat->x_min = x_mn; }
void set_rgn_y_min( Region dat, int y_mn) { dat->y_min = y_mn; }
void set_rgn_x_max( Region dat, int x_mx) { dat->x_max = x_mx; }
void set_rgn_y_max( Region dat, int y_mx) { dat->y_max = y_mx; }

int get_pixel_count( Region dat)
{
	return dat->pixel_count;
}

int get_rgn_x_min( Region dat) { return dat->x_min; }
int get_rgn_y_min( Region dat) { return dat->y_min; }
int get_rgn_x_max( Region dat) { return dat->x_max; }
int get_rgn_y_max( Region dat) { return dat->y_max; }

void inc_pixel_count( Region rgn)
{
    ++(rgn->pixel_count);
}

/******
 * update_box_coord:  Update box coordinates for a given Region
 ******/
void update_box_coord( Region rgn, int row, int col)
{
	if (col < rgn->x_min)
		rgn->x_min = col;
	else if (col > rgn->x_max)
		rgn->x_max = col;
	if (row < rgn->y_min)
		rgn->y_min = row;
	else if (row > rgn->y_max)
		rgn->y_max = row;
}

bool small_region( Region rgn)
{
	return (rgn->pixel_count <= REGION_SIZE);
}

/******
 * output_region_props:  Output region properties
 ******/
void output_region_props( Region rgn)
{
	printf( "%d: (%d,%d), (%d,%d)\n",
	        rgn->pixel_count, rgn->x_min, rgn->y_min, rgn->x_max, rgn->y_max);
}

/******
 * create_regions_list:  initialize a list of regions to NULL
 ******/
Regions create_regions_list()
{
	Regions rgns_lst = malloc( sizeof( struct regions_list));
	if (rgns_lst == NULL)
		terminate( "Error in create_regions_list: Regions list could not be created.");
	rgns_lst->head = NULL;
	rgns_lst->cur_node = rgns_lst->head;
	rgns_lst->count = 0;
	return rgns_lst;
}

/******
 * destroy_regions_list: no need to set count to zero as it is already done in
 * remove_current_region
 ******/
void destroy_regions_list( Regions rgns_lst)
{
	while (rgns_lst->head != NULL)
		remove_current_region( rgns_lst);
	free( rgns_lst);
}

void add_region( Regions rgns_lst, Region rgn_dat)
{
	struct region_node *new_node = malloc( sizeof( struct region_node));
	if (new_node == NULL)
		terminate( "Error in add_region: Region could not be added.");
	new_node->data = rgn_dat;
	new_node->next = rgns_lst->head;
	rgns_lst->head = new_node;
	++(rgns_lst->count);
}

void remove_current_region( Regions rgns_lst)
{
	struct region_node *node_ptr = rgns_lst->head;
	rgns_lst->head = rgns_lst->head->next;
	--(rgns_lst->count);
	free( node_ptr->data);
	free( node_ptr);
}

/******
 * get_current_region is used in a different context than the other two:
 * get_first_region and get_next_rgn
 ******/
Region get_current_region( Regions rgns_lst)
{
	return rgns_lst->head->data;
}

int get_regions_count( Regions rgns_lst)
{
	return rgns_lst->count;
}

Region get_first_rgn( Regions rgns_lst)
{
	rgns_lst->cur_node = rgns_lst->head;
	if (rgns_lst->cur_node == NULL)
		return NULL;
	return rgns_lst->cur_node->data;
}

Region get_next_rgn( Regions rgns_lst)
{
	if (rgns_lst->cur_node == NULL)
		terminate( "Error in get_next_rgn: Already hit end of regions list.");
	rgns_lst->cur_node = rgns_lst->cur_node->next;
	if (rgns_lst->cur_node == NULL) {
		return NULL;
	}
	return rgns_lst->cur_node->data;
}

bool end_of_rgns_list( Regions rgns_lst)
{
	return (rgns_lst->cur_node == NULL);
}

bool duplicate_rgn( Regions rgns_lst, Region rgn)
{
	if (rgn == NULL)
		terminate( "Error in duplicate_rgn: An empty region cannot be duplicate.");
	if (rgns_lst == NULL)
		terminate( "Error in duplicate_rgn: Regions list is empty.");
	struct region_node *node_ptr = rgns_lst->head;
	while (node_ptr != NULL) {
		if (rgn->x_min == node_ptr->data->x_min && rgn->y_min == node_ptr->data->y_min)
			return true;
		node_ptr = node_ptr->next;
	}
	return false;
}
