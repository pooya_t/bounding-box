#ifndef FILE_H
#define FILE_H

void open_file( FILE **rstream, char *file_name);
void read_resolution( FILE *rstream, int *col_count, int* row_count);
void close_file_stream( FILE *rstream);
void create_file( FILE **wstream, char *file_name);

#endif
