#include <stdlib.h>             /* malloc */
#include <stdbool.h>
#include <math.h>               /* ceil */
#include <stdio.h>              /* printf */
#include "sections_list.h"
#include "tracker_matrix.h"
#include "regions_list.h"
#include "terminate.h"
#include "constants.h"

struct section {
	int x_min,                  /* coordinates of top left corner */
		y_min;
	int	x_max,                  /* coordinates of bottom right corner */
		y_max;
	Tracker_matrix tmat;
	Regions rgns_lst;
};

struct section_node {
	Section data;
	struct section_node *next;
};

struct sections_list {
	struct section_node *head;
	struct section_node *cur_node;
	int count;
};

/******
 * create_section:  height AND width ARE OF THE image, NOT OF THE section!
 ******/
Section create_section( int img_height, int img_width)
{
	Section sec = malloc(sizeof( struct section));
	if (sec == NULL)
		terminate( "Error in create section: Could not create new section.");
	sec->x_min = 0;  sec->y_min = 0;
	sec->x_max = 0;  sec->y_max = 0;
	sec->tmat = create_tracker_matrix( img_height, img_width);
	load_tracker_matrix( sec->tmat);
	sec->rgns_lst = create_regions_list();
	return sec;
}

/* void destroy_section( Section sec) */
/* It MIGHT have been better if we had defined a destroy_section function and
 * called it somewhere under destroy_sections_list (instead of directly calling
 * free) */

void set_sec_x_min( Section sec, int x_mn) { sec->x_min = x_mn; }
void set_sec_y_min( Section sec, int y_mn) { sec->y_min = y_mn; }
void set_sec_x_max( Section sec, int x_mx) { sec->x_max = x_mx; }
void set_sec_y_max( Section sec, int y_mx) { sec->y_max = y_mx; }

void set_cell_visited( Section sec, int row, int col)
{
	set_tracker_cell_visited( sec->tmat, row, col);
}

int get_sec_x_min( Section sec) { return sec->x_min; }
int get_sec_y_min( Section sec) { return sec->y_min; }
int get_sec_x_max( Section sec) { return sec->x_max; }
int get_sec_y_max( Section sec) { return sec->y_max; }

bool cell_visited( Section sec, int row, int col)
{
	return tracker_cell_visited( sec->tmat, row, col);
}

void add_rgn_to_sec( Section sec, Region rgn)
{
	add_region( sec->rgns_lst, rgn);
}

Regions get_rgns_list_of_sec( Section sec)
{
	return sec->rgns_lst;
}
	
Sections create_sections_list()
{
	Sections sec_lst = malloc( sizeof( struct sections_list));
	if (sec_lst == NULL)
		terminate( "Error in create_sections_list: Could not create sections list.");
	sec_lst->head = NULL;
	sec_lst->cur_node = sec_lst->head;
	sec_lst->count = 0;
	return sec_lst;
}

void add_section( Sections sec_lst, Section sec)
{
	struct section_node *new_node = malloc( sizeof( struct section_node));
	if (new_node == NULL)
		terminate( "Error in add_section: Section could not be added.");
	new_node->data = sec;
	new_node->next = sec_lst->head;
	sec_lst->head = new_node;
	++(sec_lst->count);
}

void destroy_sections_list( Sections sec_lst)
{
	while (sec_lst->head != NULL)
		remove_current_section( sec_lst); /* sec_lst is a pointer to list on heap! */
	free( sec_lst);                       /* create_sections_list malloc */
}

void remove_current_section( Sections sec_lst)
{
	struct section_node *node_ptr = sec_lst->head;
	sec_lst->head = sec_lst->head->next;
	--(sec_lst->count);
	destroy_tracker_matrix( node_ptr->data->tmat);
	destroy_regions_list( node_ptr->data->rgns_lst);
	free( node_ptr->data);      /* create_section malloc */
	free( node_ptr);            /* add_section malloc */
}

/****** 
 * partition_image:  Partition image to FRAMED sections with a Frame width of PROX_RANGE
 ******/
Sections partition_image( int height, int width, double x)
{
	if (height < 0 || width < 0 || x < 0)
		terminate( "Error in partition_image: Image height, width, or x cannot be negative.");
	/* Create a sections_list */
	/* Split image to  2^x  sections */
	/*     Compute (x_min, y_min) and (x_max, y_max) of all sections (based on
	 *     the height and width of the image). */
	/*     Create each section (using create_section) */
	/*     Add section to sections list */
	int sec_height, sec_width;

	/* bottom-right section */
	int	x_min, x_max;//, x_int = x;

	sec_height = ceil( height / pow( 2, x/2));
	sec_width  = ceil( width / pow( 2, x/2));

	/* bottom-right section */
	x_min = width - width % sec_width;
	x_max = width - 1 + 2 * PROX_RANGE;

	/* loop over sections from bottom-right cell to top-left */
	Sections sec_list = create_sections_list();
	do {
		int y_min = height - height % sec_height,
			y_max = height - 1 + 2 * PROX_RANGE;
		do {
			Section sec = create_section( height, width);
			set_sec_x_min( sec, x_min);  set_sec_y_min( sec, y_min);
			set_sec_x_max( sec, x_max);  set_sec_y_max( sec, y_max);
			add_section( sec_list, sec);
			y_min -= sec_height;
			y_max = y_min + sec_height + 2 * PROX_RANGE - 1;
		} while (y_min > -1);
		x_min -= sec_width;
		x_max = x_min + sec_width + 2 * PROX_RANGE - 1;
	} while (x_min > -1);

	return sec_list;
}

void output_partition( Sections sec_lst)
{
	struct section_node *node_ptr = sec_lst->head;
	int x_mn, y_mn,
		x_mx, y_mx;
	while (node_ptr != NULL) {
		x_mn = node_ptr->data->x_min;
		y_mn = node_ptr->data->y_min;
		x_mx = node_ptr->data->x_max;
		y_mx = node_ptr->data->y_max;
		printf("\n(x_min, y_min) = (%d, %d)\n(x_max, y_max) = (%d, %d)\n",
		       x_mn, y_mn, x_mx, y_mx);
		node_ptr = node_ptr->next;
	} 
	printf( "\n");
}

Section get_first_sec( Sections sec_lst)
{
	if (sec_lst->head == NULL)
		terminate( "Error in get_first_sec: Empty sections list.");
	sec_lst->cur_node = sec_lst->head;
	return sec_lst->cur_node->data;
}

Section get_next_sec( Sections sec_lst)
{
	if (sec_lst->cur_node == NULL)
		terminate( "Error in get_next_sec: Already hit end of sections list.");
	sec_lst->cur_node = sec_lst->cur_node->next;
	if (sec_lst->cur_node == NULL) {
		return NULL;
	}
	return sec_lst->cur_node->data;
}

bool end_of_sec_list( Sections sec_lst)
{
	return (sec_lst->cur_node == NULL);
}

int get_sec_count( Sections sec_lst)
{
	return sec_lst->count;
}
