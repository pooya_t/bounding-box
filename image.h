#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include "sections_list.h"

typedef struct image_type *Image;

Image create_image( int height, int width);
void destroy_image( Image img);
void load_image( Image img, FILE *in_stream);
void output_image( Image img);
void output_framed_image( Image img);
void output_image_tracker( Image img);
void output_framed_image_tracker( Image img);

void add_rgn_to_img(Image img, Region rgn);
Regions get_rgns_list_of_img( Image img);
char get_pixel( Image img, int row, int col);

void output_pbm( Image img, FILE *out_stream);
void output_regions_count( Image img);

void draw_box( Image img, Region rgn);

#endif
