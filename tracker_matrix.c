#include <stdlib.h>             /* malloc */
#include <stdbool.h>
#include "tracker_matrix.h"
#include "terminate.h"
#include "constants.h"
#include "visit_const.h"

struct tracker_matrix_type {
	bool *framed_tracker_matrix; /* framed tracker matrix: mark visited pixels */
	int framed_height;
	int framed_width;
};

Tracker_matrix create_tracker_matrix( int height, int width)
{
	if (height < 0 || width < 0)
		terminate( "Error in create_tracker_matrix: Negative height or width!");
	int frmd_height = height + 2 * PROX_RANGE,
		frmd_width = width + 2 * PROX_RANGE,
		size = frmd_height * frmd_width;
	Tracker_matrix tmat = malloc( sizeof( struct tracker_matrix_type));
	if (tmat == NULL)
		terminate( "Error in create_tracker_matrix: Image could not be created.");
	tmat->framed_tracker_matrix = malloc( size * sizeof( bool));
	if (tmat->framed_tracker_matrix == NULL)
		terminate( "Error in create_tracker_matrix: Image could not be created.");
	tmat->framed_height = frmd_height;
	tmat->framed_width = frmd_width;
	return tmat;
}

void destroy_tracker_matrix( Tracker_matrix tmat)
{
    free( tmat->framed_tracker_matrix);
}

void load_tracker_matrix( Tracker_matrix tmat)
{
	int size = tmat->framed_height * tmat->framed_width;
	for (int i = 0; i < size; ++i)
		tmat->framed_tracker_matrix[ i] = UNVISITED;
}

void set_tracker_cell_visited( Tracker_matrix tmat, int row, int col)
{
	tmat->framed_tracker_matrix[ (row * tmat->framed_width) + col] = VISITED;
}

bool tracker_cell_visited( Tracker_matrix tmat, int row, int col)
{
	return tmat->framed_tracker_matrix[ (row * tmat->framed_width) + col];
}
